<?php

namespace App\Http\Controllers;

use App\Tool;
use App\Category;
use App\Tool_status;    
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Session;
use Illuminate\Support\Facades\DB;


class ToolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Tool::class);
        $categories = Category::all();
         $tools = Tool::all();
        return view('tools.index')
        ->with('tools', Tool::all())
        ->with('toolstatuses', Tool_status::all())
        ->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Tool $tool)
    {
        $this->authorize('create',$tool);
        return view('tools.create');
        // ->with('categories', Category::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Tool::class);
        $validatedData = $request->validate([
            'name'=> 'required',
            'image'=> 'required|image|max:2000',
            'description'=> 'required',
            'toolcode' => 'required|unique:tools,toolcode',
            'category_id' => 'required'
        ]);
        $tool = new Tool($validatedData);
        $path=$request->file('image');
        $path_name=time().".".$path->getClientOriginalExtension();
        $destination = "image/";
        $path->move($destination, $path_name);
        $tool->image = $destination.$path_name;

        $tool->save();
        $request->session()->put('tool.$tool->id', $tool->id);

        return back()
            ->with('message', "Tool {$tool->name} is added successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tool  $tool
     * @return \Illuminate\Http\Response
     */
    public function show(Tool $tool)
    {
        // dd($tool);
        $tools = Tool::all();
        $count = collect($tools)->where('category_id', $category_id)->where('tool_status_id',1)->count();
        $count_total = collect($tools)->where('category_id', $category_id)->count();

        return view('tools.index')->with('tool', $tool)
        ->with('toolstatuses', Tool_status::all())
        ->with('count', $count)
        ->with('count_total', $count_total)
        ->with('category', $category)
        ->with('tools', Tool::all()->where('category_id', $category->id))
        ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tool  $tool
     * @return \Illuminate\Http\Response
     */
    public function edit(Tool $tool)
    {
        $this->authorize('update', $tool);
        return view('tools.edit')
            ->with('tool', $tool)
            ->with('message', "Tool is edited successfully");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tool  $tool
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tool $tool)
    {
        // dd($request);
        $this->authorize('update', $tool);
        $validatedData = $request->validate([
            'name'=> 'required',
            'image'=> 'image|max:2000',
            'tool_status_id'=>'required'
        ]); 
        $tool->tool_status_id = $request->tool_status_id;
        $tool->update($validatedData);
        $path = $request->file('image');

        if($path != ""){

        $path_name=time().".".$path->getClientOriginalExtension();
        $destination = "image/";
        $path->move($destination, $path_name);
        $tool->image = $destination.$path_name;
        }
        $tool->save();

        return back()->with('message', "Tool is edited successfully");
         // redirect(route('tools.index', $tool->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tool  $tool
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tool $tool)
    {
        $this->authorize('delete', $tool);
        $tool->delete();
        return back()
        ->with('message', "Tool {$tool->name} is deleted successfully")
        ->with('alert', 'danger');
    }

    public function catid(Request $request)
    {
        $cat =$request->cat;

        $catid = DB::table('tools')->join('categories', 'categories.id', 'tools.category_id')->where('categories.name', $cat)->get();

        return view('tools.index',['catid' => $catid])
        ->with('tools', Tool::all())
        ->with('toolstatuses', Tool_status::all());
    }

    public function toolsCat(Request $request){
        $cat_id = $request->cat_id;
        $data = DB::table('tools')->join('categories', 'categories.id', 'tools.category_id')->where('tools.category_id', $cat_id)->get();
        dd($data);
        return view('tools.partials.tooldata',['data'=> $data])    
        ->with('tools', Tool::all())
        ->with('toolstatuses', Tool_status::all());
    }
}
