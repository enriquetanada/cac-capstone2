<?php

namespace App\Http\Controllers;

use App\Category;
use App\Tool;
use App\Tool_status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Category::class);
        
        $categories = Category::all();
         $tools = Tool::all();
        foreach($categories as $category){
            // dd($category->id);
            $category->total= DB::table('tools')->where('category_id', $category->id)->count();
            $category->save();
        }
        foreach($categories as $category){
            // dd($category->id);
            $category->available= DB::table('tools')->where('category_id', $category->id)
            ->where('tool_status_id', 1)->count();
            $category->save();
        }
        return view('categories.index')
        ->with('categories', Category::all())
        ->with('tools', Tool::all());
    }
// }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Category::class);
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Category::class);
        $validatedData = $request->validate([
            'name'=> 'required|unique:categories,name',
            'image'=> 'required|image|max:2000',
            'description'=> 'required'
        ]);
        // $path = $request->file('image')->store('public/products');
        // $url = Storage::url($path);
        $category = new Category($validatedData);
        // $category->image = $url;

        $path=$request->file('image');
        $path_name=time().".".$path->getClientOriginalExtension();
        $destination = "image/";
        $path->move($destination, $path_name);
        $category->image = $destination.$path_name;

        $category->save();
        return redirect(route('categories.index'))
        ->with('message', "Category {$category->name} is added successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {   
        $this->authorize('view', $category);
       
        $tools = Tool::all();
        $tools = Tool::all();
        $count = collect($tools)->where('category_id', $category->id)->where('tool_status_id',1)->count();
        $count_total = collect($tools)->where('category_id', $category->id)->count();
        
        $category->toolcode = strtoupper(Str::random(10));
        $category->controlno =$category->id.'-'.$category->toolcode;

        $tools= Tool::all();
        
            
        return view('categories.show')
            ->with('category', $category)
            ->with('tools', Tool::all()->where('category_id', $category->id))
            ->with('toolstatuses', Tool_status::all())
            ->with('count', $count)
            ->with('count_total', $count_total);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $this->authorize('update', $category);
        return view('categories.partials.showmodal')->with('category', $category)
        ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->authorize('update', $category);
        $validatedData = $request->validate([
            'name'=> 'required|unique:categories,name',
            'image'=> 'image|max:2000',
            'description'=> 'required'
        ]);
        $category->update($validatedData);
        $path = $request->file('image');

        if($path != ""){

        $path_name=time().".".$path->getClientOriginalExtension();
        $destination = "image/";
        $path->move($destination, $path_name);
        $category->image = $destination.$path_name;
        }
        $category->save();
        return redirect(route('categories.index', $category->id))
        ->with('message', "Category {$category->name} is edited successfully");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $this->authorize('delete', $category);

        $tools = Tool::all();
        $count_total = collect($tools)->where('category_id', $category->id)->count();
        if($count_total === 0)
            {
                $category->delete();
                return redirect(route('categories.index'))
                ->with('message',"Category {$category->name} has been deleted")
                ->with('alert','danger'); 
            }
                return back()
                ->with('message',"Category {$category->name} cannot be deleted")
                ->with('alert','danger');; 
    }

}
