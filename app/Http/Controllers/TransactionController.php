<?php

namespace App\Http\Controllers;

use App\Tool;
use App\Transaction;
use App\TransactionStatus;
use App\Tool_status;
Use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Auth;
use Illuminate\Support\Facades\DB;


class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Transaction::class);
        $transactions = Transaction::all();
        $transactionstatuses = TransactionStatus::all();
        return view('transactions.index')->with('transactions', $transactions)->with('transactionstatuses', $transactionstatuses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create',Transaction::class);

        $validatedData = $request->validate([
            'date_needed_from' => 'required',
            'date_needed_to' => 'required|after:date_needed_from'
        ]);
        
        $transaction = new Transaction;
        $transaction->transaction_code = strtoupper(Str::random(10));
        $transaction->date_needed = $request->date_needed_from;
        $transaction->date_return = $request->date_needed_to;
        $transaction->user_id = Auth::user()->id;
        $transaction->save();
        // dd(session('tool'));
        $tools = Tool::find(array_keys(session('tool')));
        foreach($tools as $tool){

            // dd($tool);
            $toolcode = $tool->toolcode;
            
            $status =$tool->tool_status_id ;
            // dd("$status");
            $transaction->tools()->attach($tool->id,[
                'toolcode'=>$toolcode,
                'status' => $status
            ]);

        }
         $transaction->save();
        session()->forget('tool');

        return redirect(route('transactions.show',$transaction->id))->with('toolstatuses', Tool_status::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction, TransactionStatus $transactionstatus)
    {
        $this->authorize('view', $transaction);

        return view('transactions.show')->with('transaction', $transaction)->with('transactionstatuses', TransactionStatus::all())->with('transactions', Transaction::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        $this->authorize('update', $transaction);   
        $transaction->transaction_status_id = $request->status_id;
        $transaction->save();
        $items=DB::table('tool_transaction')->get();
        foreach($items as $item){
                
            if($transaction->transaction_status_id==="3"){
                    foreach ($transaction->tools as $tool)
                    {    
                        if($item->tool_id === $tool->id){
                            if($tool->tool_status_id === 1)
                            {
                                DB::table('tools')->where("id", $item->tool_id )->increment('tool_status_id');
                            } 
                        }
                    }  
            }

            else
            {
                    foreach ($transaction->tools as $tool)
                    {    
                        if($item->tool_id === $tool->id){
                            if($tool->tool_status_id !== 1)
                            {
                                DB::table('tools')->where("id", $item->tool_id )->decrement('tool_status_id');
                            } 
                        }
                    }
            }
    }
    
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        
    }
}
