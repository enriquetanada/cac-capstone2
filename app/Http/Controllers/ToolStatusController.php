<?php

namespace App\Http\Controllers;

use App\Tool_status;
use Illuminate\Http\Request;

class ToolStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tool_status  $tool_status
     * @return \Illuminate\Http\Response
     */
    public function show(Tool_status $tool_status)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tool_status  $tool_status
     * @return \Illuminate\Http\Response
     */
    public function edit(Tool_status $tool_status)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tool_status  $tool_status
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tool_status $tool_status)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tool_status  $tool_status
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tool_status $tool_status)
    {
        //
    }
}
