<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tool_status extends Model
{
    public function tools(){
    	return $this->hasMany('App\Tool');
    }
}
