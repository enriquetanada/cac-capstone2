<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Tool extends Model
{
	use SoftDeletes;
	protected $fillable = ['name', 'image', 'description', 'toolcode', 'category_id'];

	public function category()
    {
    	return $this->belongsTo('App\Category');
    }
	public function tool_status()
    {
    	return $this->belongsTo('App\Tool_status');
    }
    
    
}
