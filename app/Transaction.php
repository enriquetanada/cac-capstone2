<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function transaction_status(){
    	return $this->belongsTo('App\TransactionStatus');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function tools(){
    	return $this->belongsToMany('App\Tool')->withPivot('toolcode', 'status')->withTimestamps();
    }
}
