<?php

namespace App\Policies;

use App\Tool;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ToolPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->role_id===1|| $user->role_id===2;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Tool  $tool
     * @return mixed
     */
    public function view(User $user, Tool $tool)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role_id === 1;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Tool  $tool
     * @return mixed
     */
    public function update(User $user, Tool $tool)
    {
        return $user->role_id === 1;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Tool  $tool
     * @return mixed
     */
    public function delete(User $user, Tool $tool)
    {
        return $user->role_id === 1;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Tool  $tool
     * @return mixed
     */
    public function restore(User $user, Tool $tool)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Tool  $tool
     * @return mixed
     */
    public function forceDelete(User $user, Tool $tool)
    {
        //
    }
}
