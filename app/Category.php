<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
	use SoftDeletes;
    protected $fillable = ['name', 'image', 'description'];

    public function tools(){
    	return $this->hasMany('App\Tool');
    }
}
