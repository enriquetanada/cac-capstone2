<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ToolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tools')->insert([
        	'name'=> 'compass',
        	'image'=> 'https://1oomzzme3s617r8yzr8qutjk-wpengine.netdna-ssl.com/wp-content/uploads/2019/10/WHTH_FAQ_fluxgate-compass_Pt1_Fig1.png',
        	'description' => '“ancient” magnetic-needle compass',
        	'category_id'=> 1,
        	'toolcode' => 'A36CDDFCWP'
        ]);
        DB::table('tools')->insert([
        	'name'=> 'tent',
        	'image'=> 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQDnuXI8f9VQpfN6LIUhRGHpfS5OBezl_lmrg&usqp=CAU',
        	'description' => 'blue orange tent',
        	'category_id'=> 1,
        	'toolcode' => 'BNOAPDHELF'
        ]);
        DB::table('tools')->insert([
        	'name'=> 'Grill Pan',
        	'image'=> 'https://images-na.ssl-images-amazon.com/images/I/4120gKZ5DQL._AC_SX466_.jpg',
        	'description' => 'BBQ Pans Black',
        	'category_id'=> 2,
        	'toolcode' => 'DKXPAJFMCL'
        ]);
        DB::table('tools')->insert([
            'name'=> 'Paint Brush',
            'image'=> 'https://www.artnews.com/wp-content/uploads/2020/05/shutterstock_430316950.jpg?w=682',
            'description' => 'Paint Brush long',
            'category_id'=> 3,
            'toolcode' => 'APCJELFLAR'
        ]);
        DB::table('tools')->insert([
            'name'=> 'Artbook',
            'image'=> 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTr_bQO26zNAO7HTtLPDsBFacqnbOVloh_E2Q&usqp=CAU',
            'description' => 'Artbook',
            'category_id'=> 4,
            'toolcode' => 'PAOWJDJALD'
        ]);
        DB::table('tools')->insert([
            'name'=> 'First Aid Kit',
            'image'=> 'https://medcenterblog.uvmhealth.org/wp-content/uploads/2016/07/iStock_86048769_LARGE.jpg',
            'description' => 'BandAid',
            'category_id'=> 5,
            'toolcode' => 'APCENSDFKHD'
        ]);


    }
}
