<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ToolStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tool_statuses')->insert([
        	'name' => 'available'
        ]);
        DB::table('tool_statuses')->insert([
        	'name' => 'not available'
        ]);
        DB::table('tool_statuses')->insert([
        	'name' => 'under maintenance'
        ]);
    }
}
