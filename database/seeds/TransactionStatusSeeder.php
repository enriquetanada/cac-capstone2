<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransactionStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transaction_statuses')->insert([
        	'name'=> 'Pending'
        ]);
        DB::table('transaction_statuses')->insert([
        	'name'=> 'Decline'
        ]);
        DB::table('transaction_statuses')->insert([
        	'name'=> 'Approved'
        ]);
        DB::table('transaction_statuses')->insert([
        	'name'=> 'Completed'
        ]);
    }
}
