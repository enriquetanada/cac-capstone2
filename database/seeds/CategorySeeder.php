<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
        	'name'=> 'Hiking',
        	'image'=>'https://www.blackhillsinfosec.com/wp-content/uploads/2017/07/mtn_tools_sm.jpg',
        	'description' => 'Hiking tools',
            'available' => 0,
            'total' =>0
        ]);
        DB::table('categories')->insert([
        	'name'=> 'Cooking',
        	'image'=>'https://www.trail.recipes/wp-content/uploads/sites/18/2017/05/backpacking-kitchen-set.jpg',
        	'description' => 'Cooking tools',
            'available' => 0,
            'total' =>0
        ]);
        DB::table('categories')->insert([
        	'name'=> 'Painting',
        	'image'=>'https://thumbs.dreamstime.com/z/set-different-painting-tools-house-renovation-flat-lay-set-different-painting-tools-house-renovation-white-wooden-117190332.jpg',
        	'description' => 'Painting tools',
            'available' => 0,
            'total' =>0
        ]);
        DB::table('categories')->insert([
            'name'=> 'Teaching',
            'image'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQpMlWg6rw6lvgt8Y0SBZUjifkMvhUObbiY-w&usqp=CAU',
            'description' => 'Teaching tools',
            'available' => 0,
            'total' =>0
        ]);
        DB::table('categories')->insert([
            'name'=> 'Medical',
            'image'=>'https://previews.123rf.com/images/jovanjaric/jovanjaric1504/jovanjaric150400007/44746504-open-first-aid-kit-with-bandages-scissors-triangle-scarf-syringe-plaster-knife-tools-gauze-etc.jpg',
            'description' => 'First Aid KIt tools',
            'available' => 0,
            'total' =>0
        ]);
        
    }
}
