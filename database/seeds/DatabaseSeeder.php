<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
        	CategorySeeder::class,
        	RoleSeeder::class,
            ToolStatusSeeder::class,  
            ToolSeeder::class,
            UserSeeder::class,
            TransactionSeeder::class,
            TransactionStatusSeeder::class
              
        ]);
    }
}
