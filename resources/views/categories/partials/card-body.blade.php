{{-- image start--}}
<img src="{{$category->image}}"alt="" class="card-img-top my-1" id="sample" height="200" width="250">
	{{-- image end --}}
<div class="card-body text-dark">
	{{-- category name --}}
	<div class="card-title text-center">
		<h4 class="card-name">
			{{$category->name}}
		</h4>
		<h5>Current Available:<strong> {{$count}}/{{$count_total}}</strong></h5>
	</div>
	{{-- end category name --}}

	{{-- description start --}}
	<div class="card-text text-center">
		<small>{{$category->description}}</small>
	</div>
	{{-- description end --}}
</div>