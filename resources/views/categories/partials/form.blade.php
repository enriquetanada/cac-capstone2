
<label for="name">Category name:</label>
<input 
	type="text" 
	name="name" 
	id="name" 
	class="form-control form-control-sm 
	@error('name') is-invalid @enderror" 
	required
	>
	@error('name')
		<small class="d-block invalid-feedback">
			<strong>{{$message}}</strong>
		</small>
	@enderror
<label for="image">Category image:</label>
<input type="file" name="image" id="image" class="form-control-file form-control-sm" required>

<label for="description">Category description:</label>
<textarea name="description" id="description" cols="30" rows="10" class="form-control form-control-sm" required></textarea>
<button class="btn btn-sm btn-primary mt-3">
	Add Category
</button>
