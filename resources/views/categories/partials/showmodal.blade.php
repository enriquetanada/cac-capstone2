<!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#staticBackdrop">
  Edit
</button> --}}
<a href="{{route('categories.edit', $category->id)}} {{-- should be directed to product --}}" class="btn btn-sm btn-outline-primary w-100" data-toggle="modal" data-target="#staticBackdrop{{$category->id}}">Edit</a>

<!-- Modal -->
<div class="modal fade" id="staticBackdrop{{$category->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel{{$category->id}}" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel{{$category->id}}">Edit Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('categories.update', $category->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    {{-- start form --}}
                        <label for="name">Category name:</label>
                        <input 
                            type="text" 
                            name="name" 
                            id="name" 
                            class="
                                form-control 
                                form-control-sm
                                @error('name') is-invalid @enderror
                                "  
                            value="{{$category->name}}"
                        >
                        @error('name')
                            <small class="d-block invalid-feedback">
                                <strong>{{$message}}</strong>
                            </small>
                        @enderror
                        <label for="image">Category image:</label>
                        <input type="file" name="image" id="image" class="form-control-file form-control-sm @error('image') is-invalid @enderror">
                        @error('image')
                            <small class="d-block invalid-feedback">
                                <strong>{{$message}}</strong>
                            </small>
                        @enderror
                        <label for="description">Category description:</label>
                        <textarea name="description" id="description"class="form-control form-control-sm @error('description') is-invalid @enderror" >{{$category->description}}</textarea>
                        @error('description')
                            <small class="d-block invalid-feedback">
                                <strong>{{$message}}</strong>
                            </small>
                        @enderror
                        {{-- <button class="btn btn-primary">Edit Category</button> --}}
                    
                  {{-- end form --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-outline-info">Edit Category</button>
                </div>
            </form>
        </div>
    </div>
</div>