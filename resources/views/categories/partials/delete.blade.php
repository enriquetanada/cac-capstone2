<form action="{{route('categories.destroy', $category->id)}}" method="post" class=" my-1">
	@csrf
	@method('DELETE')
	<button class="btn btn-sm btn-outline-danger w-100">Delete</button>
</form>