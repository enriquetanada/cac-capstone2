@extends('layouts.app')

@section('content')
	<div class="container-fluid">
		@includeWhen(Session::has('message'), 'partials.alert')
		@can('isAdmin')
		<div class="row">
		  	<div class="col-3 my-1">
		    	<div class="list-group" id="list-tab" role="tablist">
		      		<a 
		      			class="list-group-item list-group-item-action active" 
		      			id="list-home-list" 
		      			data-toggle="list" 
		      			href="#list-home" 
		      			role="tab" 
		      			aria-controls="home"
		      		>
		      			Categories
		      		</a>

		     		<a 
		     			class="list-group-item list-group-item-action" 
		     			id="list-profile-list" 
		     			data-toggle="list" 
		     			href="#list-profile" 
		     			role="tab" 
		     			aria-controls="profile"
		     		>
		     		Create Category
		     		</a>	
		    	</div>
		  	</div>
		@endcan
		  	<div class="col-9 mx-auto">
		    	<div class="tab-content" id="nav-tabContent">
		      		<div 
		      			class="tab-pane fade show active" 
		      			id="list-home" 
		      			role="tabpanel" 
		      			aria-labelledby="list-home-list"
		      		>
						@foreach($categories as $category)
							<?php 
								$count = collect($tools)
									->where('category_id', $category->id)
									->where('tool_status_id',1)
									->count();
								$count_total = collect($tools)
									->where('category_id', $category->id)
									->count();
							?>
							{{-- card start --}}
								<div class="card border-secondary text-center col-lg-3 col-md-3 col-sm-6 d-inline-block mx-3 my-1">
									@include('categories.partials.card-body')
									<div class="card-footer bg-transparent">
										<a href="{{route('categories.show', $category->id)}}" class="btn btn-sm btn-outline-success my-1 w-100">View</a>	
										@can('isAdmin')
											{{-- showmodal --}}
											@include('categories.partials.showmodal')
											{{-- delete --}}
											@include('categories.partials.delete')
										@endcan
									</div>	
								</div>
							{{-- card end --}}
						@endforeach
					</div>

		     		<div 
		     			class="tab-pane fade" 
		     			id="list-profile" 
		     			role="tabpanel" 
		     			aria-labelledby="list-profile-list"
		     		>
		     			<div class="col-9">
		     				<h1 class="text-center">
		     					Add Category
		     				</h1>
		     				{{-- start form --}}
								<form action="{{route('categories.store')}}" method="post" enctype="multipart/form-data">
									@csrf
									@include('categories.partials.form')	
								</form>
							{{-- end form --}}
		     			</div>
		     		</div>
		    	</div>
		  	</div>
		</div>
	</div>
@endsection


