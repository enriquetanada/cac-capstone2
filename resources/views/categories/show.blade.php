@extends('layouts.app')

@section('content')
	<div class="container">
		@includeWhen(Session::has('message'), 'partials.alert')
		<div class="row">
			<div class="col-3 col-sm-5 col-lg-3">
				<div class="card border-secondary text-center mx-2 my-1">
					<div class="card-body text-dark">
						<div class="card-body text-dark">
							{{-- category name --}}
							<div class="card-title text-center">
								<h4 class="card-name">
									{{$category->name}}
								</h4>
								<br>
								<h6>Current Available: <strong>{{$count}}/{{$count_total}}</strong></h6>
							</div>
							{{-- end category name --}}

							{{-- description start --}}
							<div class="card-text text-center">
								<small>{{$category->description}}</small>
							</div>
							{{-- description end --}}
						</div>
					</div>
					<div class="card-footer">		
						@can('isAdmin')
						{{-- showmodal --}}
						@include('categories.partials.showmodal')

						{{-- delete --}}
						@include('categories.partials.delete')
						@endcan
					</div>

				</div>
				@can('isAdmin')
				<h4 class="text-center">Control code</h4>
				<form action="{{route('tools.store')}}" method="post" enctype="multipart/form-data">
					@csrf
					<div class="input-group mb-3">
					 	<div class="input-group-prepend">
					    {{-- <span class="input-group-text" id="basic-addon1" name="">{{$category->id}}-</span> --}}
					</div>
					  	<input type="text" class="form-control " name="toolcode" id="toolcode" value="{{$category->controlno}}" readonly>
					</div>
					<div class="d-block">
						<h4 class="text-center">Tool Model</h4>
						<select name="category_id" id="category_id" class="form-control form-control-sm" readonly>
						<option value="{{$category->id}}">{{$category->name}} </option>
						</select>
					</div>
					<div class="d-block">
						<h4 class="text-center">Tool Name</h4>
						 <input type="text" name="name" id="name" class="form-control form-control-sm">
						 <input type="file" name="image" id="image" class="form-control-file form-control-sm">
						<h4 class="text-center">Tool Description</h4>
                        <textarea name="description" id="description" class="form-control form-control-sm"></textarea>
                        <button class="btn btn-outline-dark text-center my-1 w-100">Create Tool</button>
					</div>	
				</form>
				@endcan
			</div>
			<div class="col-9 col-sm-7 col-lg-9	">
				@foreach($tools as $tool)
					@if ($category->id === $tool->category_id)
							{{-- product card start --}}
							<div class="col-12 col-lg-3 d-inline-block mx-1 my-1 text-center card-category">
								<div class="card  border-secondary">
									<img src="{{asset($tool->image)}}" alt="" class="card-img-top my-1" height="150" width="250">
									<div class="card-body text-dark">
										<h5 class="card-title ">
											{{$tool->name}}
										</h5>
										<p class="card-text">
											{{$tool->toolcode}}
										</p>
										<p class="card-text mb-0">
											<span class="badge badge-info">
												{{$tool->category->name}}
											</span>
										</p>
										<p class="card-text mb-0">
											<span class="badge badge-{{$tool->tool_status_id === 1 ? "success" : ($tool->tool_status_id === 2 ? "info" : "warning")}}">
														{{$tool->tool_status->name}}
											</span>
										</p>
										@cannot('isAdmin')
										<form action="{{route('requests.update', $tool->id)}}" method="post">
											@csrf
											@method('PUT')
											<button class="
													btn 
													btn-sm 
													btn-outline-{{$tool->tool_status_id!==1 ? "dark" : "success"}} w-100 mt-1" 
													{{$tool->tool_status_id!==1 ? "disabled" : ""}}
													>{{$tool->tool_status_id!==1 ? "Not Available" : "Request"}}
									</button>
										</form>
										@endcannot
									</div>
									@can('isAdmin')
										<div class="card-footer bg-transparent text-center">
											@include('tools.partials.editmodal')
											@include('tools.partials.delete-form')
										</div>
									@endcan
								</div>
							</div>
							{{-- product card end --}}
					@endif
				@endforeach
			</div>
		</div>
	</div>
@endsection
