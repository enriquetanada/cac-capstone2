@extends('layouts.app')
@section('content')
@auth
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center">
                    My Request
                </h1>
            </div>
        </div>

        {{-- alert start --}}
        @if(!Session::has('tool'))
            <div class="alert alert-info alert-dismissible fade show text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>Your cart is empty!</strong> 
            </div>
            {{-- alert end --}}
        @else
        
        <div class="row">
            <div class="col-12">
                {{-- start cart table --}}

                <table class="table">
                    <thead>
                        <tr>
                            <th>Tool</th>
                            <th>Control Code</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tools as $tool)
                            {{-- start item row --}}
                            <tr>
                                <td scope="row">{{$tool->name}}</td>
                                <td>{{$tool->toolcode}}</td>
                                <td>{{$tool->tool_status->name}}</td>
                                <td>
                                    <form action="{{route('requests.destroy', $tool->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-sm btn-outline-danger">Remove</button>
                                    </form>
                                </td>
                            </tr>
                            {{-- end item row --}}
                        @endforeach
                    </tbody>
                    <tfoot>
                        {{-- <tr>
                            <td colspan="3" class="text-right">Total</td>
                            <td><strong>&#8369; $total</strong></td>
                            <td >
                                <a href="#" class="btn btn-sm w-100 btn-success">Checkout</a>
                                
                            </td>
                        </tr> --}}
                    </tfoot>
                </table>
            </div>
        </div>
         {{-- end cart table --}}
         {{-- start date --}}
        <div class="row">
	        <div class="col-12 col-sm-8 mx-auto">

	            <form action="{{route('transactions.store')}}" method="POST" class="mb-5">
                    @csrf               
	                <div class="row align-items-center">
	                    <div class="col align-self-start">
	                        
	                        <div class="form-group">
	                            <label for="date_needed_from">Date needed</label>
	                            <input required="" type="date" min="<?=date('Y-m-d',strtotime("+1 day"));?>" name="date_needed_from" id="date_needed_from" class="form-control" placeholder="Date Needed" aria-describedby="dateNeededFrom">
	                            <small id="dateNeededFrom" class="text-muted">Date when you need to have this tool</small>
	                        </div>
	                    </div>
	                    <div class="col align-self-start">
	                        
	                        <div class="form-group">
	                            <label for="date_needed_to">Date to return</label>
	                            <input required="" type="date" min="<?=date('Y-m-d',strtotime("+3 day"));?>" max="<?=date('Y-m-d',strtotime("+15 day"));?>"name="date_needed_to" id="date_needed_to" class="form-control @error('date_needed_to') is-invalid @enderror" placeholder="Date Needed" aria-describedby="dateNeededFrom">
	                            <small id="dateNeededFrom" class="text-muted">Date when you will return these tools</small> 
                                @error('date_needed_to')
                                    <small class="d-block invalid-feedback">
                                        <strong>{{$message}}</strong>
                                    </small>
                                @enderror
	                        </div>
	                    </div>
	                </div>
                     @guest
                    <a href="{{route('login')}}" class="btn btn-sm btn-success w-100 my-1">Login to request</a>
                    @else
                        <button class="btn btn-sm btn-success w-100">Request</button>
                    @endguest
                
                    </form>
	                
                   
	        </div>
    	</div>
    	{{-- end date --}}
        {{-- clear cart --}}
        
        <div class="row">
            <div class="col-12">
                <form action="{{route('requests.delete')}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-sm btn-outline-danger">Clear Request</button>
                </form>
            </div>
        </div>
        @endif
    </div>
@endauth  
@endsection