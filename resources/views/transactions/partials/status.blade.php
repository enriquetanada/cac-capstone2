<div class="accordion" id="accordionExample">
@cannot('isAdmin')
  @foreach($transactions as $transaction)
    @if($transaction->user_id ===Auth::user()->id)
        @include('transactions.partials.accordion-data')
      @endif
  @endforeach
@endcannot
@can('isAdmin')
  @foreach($transactions as $transaction)
        @include('transactions.partials.accordion-data')
  @endforeach
@endcan
</div>