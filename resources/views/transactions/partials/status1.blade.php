<div class="accordion" id="accordionExample">
@cannot('isAdmin')
  @foreach($transactions as $transaction)
    @if($transaction->user_id ===Auth::user()->id)
      @if($transaction->transaction_status_id === 1)
        @include('transactions.partials.accordion-data')
      @endif
    @endif 
  @endforeach
@endcannot
@can('isAdmin')
  @foreach($transactions as $transaction)
    @if($transaction->transaction_status_id === 1)
        @include('transactions.partials.accordion-data')
    @endif
  @endforeach
@endcan
</div>