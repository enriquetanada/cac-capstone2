{{-- table start --}}
<div class="table-responsive">
	<table class="table table-hover">
		{{-- Date start --}}
		<tr>
			<td>
				<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-calendar2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
				<path fill-rule="evenodd" d="M14 2H2a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1zM2 1a2 2 0 0 0-2 2v11a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2z"/>
				<path fill-rule="evenodd" d="M3.5 0a.5.5 0 0 1 .5.5V1a.5.5 0 0 1-1 0V.5a.5.5 0 0 1 .5-.5zm9 0a.5.5 0 0 1 .5.5V1a.5.5 0 0 1-1 0V.5a.5.5 0 0 1 .5-.5z"/>
				<path d="M2.5 4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V4z"/>
				</svg> 
				Date Requested
			</td>
			<td>{{date("M d, Y - H:i:s", strtotime($transaction->created_at))}}</td>
		</tr>
		{{-- Date end --}}
		
		{{-- Customer name start --}}
		<tr>
			<td>
				<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  				<path fill-rule="evenodd" d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
				</svg>
				Customer Name
			</td>
			<td>{{$transaction->user->name}}</td>
		</tr>
		{{-- Customer name end --}}

		{{-- Status start--}}
		
		<tr>
			<td>
				<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-card-checklist" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
				<path fill-rule="evenodd" d="M14.5 3h-13a.5.5 0 0 0-.5.5v9a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z"/>
				<path fill-rule="evenodd" d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm-1.496-.854a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0zM7 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm-1.496-.854a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 0 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0z"/>
				</svg>
				Status
			</td>
			<td>
				{{$transaction->transaction_status->name}}
				@can('isAdmin')
				@include('transactions.partials.editstatus')
				@endcan()
			</td>
		</tr>
		{{-- Status end --}}

		{{-- date needed start --}}
		<tr>
			<td>
				<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-calendar2-date" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
				<path d="M6.445 12.438V7.104h-.633A12.6 12.6 0 0 0 4.5 7.91v.695c.375-.257.969-.62 1.258-.777h.012v4.61h.675zm1.188-1.305c.047.64.594 1.406 1.703 1.406 1.258 0 2-1.066 2-2.871C11.336 7.734 10.555 7 9.383 7c-.926 0-1.797.672-1.797 1.809 0 1.16.824 1.77 1.676 1.77.746 0 1.23-.376 1.383-.79h.027c-.004 1.316-.461 2.164-1.305 2.164-.664 0-1.008-.45-1.05-.82h-.684zm2.953-2.317c0 .696-.559 1.18-1.184 1.18-.601 0-1.144-.383-1.144-1.2 0-.823.582-1.21 1.168-1.21.633 0 1.16.398 1.16 1.23z"/>
				<path fill-rule="evenodd" d="M14 2H2a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1zM2 1a2 2 0 0 0-2 2v11a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2z"/>
				<path fill-rule="evenodd" d="M3.5 0a.5.5 0 0 1 .5.5V1a.5.5 0 0 1-1 0V.5a.5.5 0 0 1 .5-.5zm9 0a.5.5 0 0 1 .5.5V1a.5.5 0 0 1-1 0V.5a.5.5 0 0 1 .5-.5z"/>
				<path d="M2.5 4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V4z"/>
				</svg>
				Date needed
			</td>
			<td>{{date("M d, Y", strtotime($transaction->date_needed))}}</td>
		</tr>
		{{-- date needed end --}}

		

		{{-- date return start --}}
		<tr>
			<td>
			<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-calendar3-range" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			<path fill-rule="evenodd" d="M14 0H2a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"/>
			<path fill-rule="evenodd" d="M7 10a1 1 0 0 0 0-2H1v2h6zm2-3a1 1 0 0 1 0-2h6v2H9z"/>
			</svg>
			Date to return
		</td>
			<td>{{date("M d, Y", strtotime($transaction->date_return))}}</td>
		</tr>
		{{--date return end --}}
		

	</table>
	<table class="table table-hover">
		
		<thead>
			<tr>
				<th>Tool Name</th>
				<th>Tool Code</th>
				<th>Tool Returned Date</th>
				<th>Current Unit Status</th>
			</tr>
		</thead>
		<tbody>
			{{-- Tool start  --}}
			@foreach ($transaction->tools as $tool)
			<tr>
				<td>{{$tool->name}}</td>
				<td>{{$tool->pivot->toolcode}}</td>
				<td>{{date("M d, Y", strtotime($transaction->date_return))}}</td>
				<td>{{$tool->tool_status->name}}</td>
			</tr>
			@endforeach

		{{-- Tool end --}}
		</tbody>
		
	</table>
</div>

