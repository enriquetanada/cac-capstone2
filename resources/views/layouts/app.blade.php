<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="css/home.blade.css" rel="stylesheet" type="text/css">
</head>
<body class="test">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark shadow-sm " style="background-color: #BD815C;">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        {{-- category index --}}
                        @auth
                        <li class="
                            nav-item
                            {{Route::CurrentRouteNamed('categories.index') ? "active" : ""}}
                            ">
                            <a href="{{route('categories.index')}}" class="nav-link">Categories</a>
                        </li>
                        @endauth
                        {{-- end category index --}}

                        {{-- start tool index --}}
                        @auth
                        <li class="
                            nav-item
                            {{Route::CurrentRouteNamed('tools.index') ? "active" : ""}}
                            ">
                            <a href="{{route('tools.index')}}" class="nav-link">Tools</a>
                        </li>
                        {{-- end tool index --}}
                        @endauth
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @auth
                        <li class="nav-item">
                            <a href="{{route('transactions.index')}}" class="nav-link">Requests</a>
                        </li>
                        {{-- cart start --}}
                        @cannot('isAdmin')
                        <li class="
                            nav-item
                            {{Route::CurrentRouteNamed('requests.index') ? "active" : "" }}
                            ">
                            <a href="{{route('requests.index')}}" class="nav-link">
                                Request Form
                                <span class="badge badge-primary">
                                    {{Session::has('tool') ? count(Session::get('tool')): 0}}
                                </span>
                            </a>
                        {{-- cart end  --}}
                        </li>
                        @endcannot
                        @endauth
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
