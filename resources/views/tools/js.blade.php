<script type='text/javascript'>
	$(document).ready(function(){
		@foreach($categories as $category)
		$("#cat{{$category->id}}").click(function(){
			var cat= $("#cat{{$category->id}}").val();
			alert(cat);

			$.ajax({
				type: 'get',
				url: '{{url('/toolsCat')}}',
				dataType: 'html',
				data: 'cat_id=' +cat,
				success:function(response){
					console.log(response);
					$("#toolData").html(response);
				}
			});
		});
		@endforeach
	});
</script>
