@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-3">
				<h3 class="text-center">
					
				</h3>
			</div>
		</div>
		@includeWhen(Session::has('message'), 'partials.alert')
			<div class="row">
			@foreach($tools as $tool)
				<div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-2 mx-auto">
					{{-- product card start --}}
					<div class="card border-secondary">
						<img src="{{asset($tool->image)}}" alt="" class="card-img-top my-1" height="100" width="250">
						<div class="card-body text-dark text-center">
							<h5 class="card-title">
								{{$tool->name}}
							</h5>
							<p class="card-text">
								{{$tool->toolcode}}
							</p>
							<p class="card-text mb-0">
								<span class="badge badge-info">
									{{$tool->category->name}}
								</span>
							</p>
							<p class="card-text mb-0">
								<span class="badge badge-{{$tool->tool_status_id === 1 ? "success" : ($tool->tool_status_id === 2 ? "info" : "warning")}}">
									{{$tool->tool_status->name}}
								</span>
							</p>
							@cannot('isAdmin')
							<form action="{{route('requests.update', $tool->id)}}" method="post">
									@csrf
									@method('PUT')
									<button class="
										btn btn-sm btn-outline-{{$tool->tool_status_id!==1 ? "dark" : "success"}} w-100 mt-1" 
										{{$tool->tool_status_id!==1 ? "disabled" : ""}}
										>{{$tool->tool_status_id!==1 ? "Not Available" : "Request"}}
									</button>
							</form>
							@endcannot
						</div>
						@can('isAdmin')
							<div class="card-footer bg-transparent text-center">
								@include('tools.partials.editmodal')
								@include('tools.partials.delete-form')
							</div>
						@endcan
					</div>
					{{-- product card end --}}
				</div>
			@endforeach
		</div>
	</div>
@endsection