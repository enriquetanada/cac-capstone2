<a href="{{route('tools.edit', $tool->id)}} {{-- should be directed to product --}}" class="btn btn-sm btn-outline-primary w-100" data-toggle="modal" data-target="#staticBackdrop{{$tool->id}}">Edit</a>

<!-- Modal -->
<div class="modal fade" id="staticBackdrop{{$tool->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel{{$tool->id}}" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel{{$tool->id}}">Edit Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('tools.update', $tool->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    {{-- start form --}}
                        <label for="name">Tool name:</label>
                        <input type="text" name="name" id="name" class="form-control form-control-sm" required value="{{$tool->name}}">
                        {{-- <button class="btn btn-primary">Edit Category</button> --}}
                        <label for="toolcode">Control code:</label>
                        <input type="text" class="form-control " placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" name="toolcode" id="toolcode" value="{{$tool->toolcode}}" disabled>
                        <label for="toolcode">Status:</label>
                       <select name="tool_status_id" id="tool_status_id" class="form-control form-control-sm">
                            @foreach($toolstatuses as $toolstatus)
                            <option value="{{$toolstatus->id}}"{{$toolstatus->id === $tool->tool_status_id ? "selected" : ""}}>{{$toolstatus->name}}</option>
                            @endforeach
                    </select>

                  {{-- end form --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-outline-info">Edit Tool</button>
                </div>
            </form>
        </div>
    </div>
</div>